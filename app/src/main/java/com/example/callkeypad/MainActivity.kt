import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.callkeypad.R
import kotlinx.android.synthetic.main.activity_main.*

@Suppress("PLUGIN_WARNING")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var x = 0
        input_num.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int,count: Int, after: Int) {
                Add_num.visibility = View.INVISIBLE
            }
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(input_num.text.isNotEmpty())
                    Add_num.visibility = View.VISIBLE
            }
        })

        input_num.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int,count: Int, after: Int) {
                clear.visibility = View.INVISIBLE
            }
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(input_num.text.isNotEmpty()) {
                    clear.visibility = View.VISIBLE
                }
            }
        })
        btn1.setOnClickListener {
            input_num.append('1'.toString())
            x+=1
        }
        btn2.setOnClickListener{
            input_num.append('2'.toString())
            x+=1
        }
        btn3.setOnClickListener{
            input_num.append('3'.toString())
            x+=1
        }
        btn4.setOnClickListener {
            input_num.append('4'.toString())
            x+=1
        }
        btn5.setOnClickListener {
            input_num.append('5'.toString())
            x+=1
        }
        btn6.setOnClickListener {
            input_num.append('6'.toString())
            x+=1
        }
        btn7.setOnClickListener {
            input_num.append('7'.toString())
            x+=1
        }
        btn8.setOnClickListener {
            input_num.append('8'.toString())
            x+=1
        }
        btn9.setOnClickListener {
            input_num.append('9'.toString())
            x+=1
        }
        btn0.setOnClickListener {
            input_num.append('0'.toString())
            x+=1
        }
        btn_str.setOnClickListener {
            input_num.append('*'.toString())
            x+=1
        }
        btn_sharp.setOnClickListener {
            input_num.append('#'.toString())
            x+=1
        }
       /* clear.setOnClickListener {
           input_num.text.dropLast(1)
            // input_num.text.removeRange(x-1,x)
        }*/
    }
}

